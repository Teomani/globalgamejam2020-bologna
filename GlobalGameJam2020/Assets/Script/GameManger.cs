﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameManger : MonoBehaviour
{

    public bool isSkippable;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isSkippable)
        {
            if (Input.GetButtonDown("Cancel"))
            {
                GameObject.Find("Fader").GetComponent<DOTweenAnimation>().DOPlayById("FadeOut");
            }
        }
    }

    public void LoadSceneByInt(int scenInt)
    {
        SceneManager.LoadScene(scenInt);
        //La scena viene caricata sulla base di quanto inserito
    }

    public void CloseGame()
    {
        Application.Quit();
        //Il gioco si chiude
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //La scena di gioco viene ricaricata
    }
}
