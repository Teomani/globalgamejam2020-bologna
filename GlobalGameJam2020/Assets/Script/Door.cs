﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool isLocked;
    public Transform roomEntry;
    public Transform cameraAncor;
    public LayerMask whatIsRoom;


    //
    public Sprite OpenDoor, ClosedDoor;

    void Start()
    {


        roomEntry = transform.Find("Entry Point").GetComponent<Transform>();

        Collider2D hitInfo = Physics2D.OverlapCircle(roomEntry.position, 0.5f, whatIsRoom);

        if (hitInfo)
        {
            cameraAncor = hitInfo.transform.Find("Camera Ancor").transform;

        }
        else
        {
            isLocked = true;
        }

        ChangeSprite();
    }


    void ChangeSprite()
    {
        if (isLocked)
        {
            GetComponent<SpriteRenderer>().sprite = ClosedDoor;
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = OpenDoor;

        }
    }
}
