﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SwordController : MonoBehaviour
{
    public Transform swordTransform;
    public Vector2 movmentVector; //Direzione di movimento //
    public float attackRange = 2;
    public Transform swordBlade;

    public float swordAngle;
    public int damage;
    public LayerMask whatIsEnemy;

    //
    public DOTweenAnimation dOTweenAnimation;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(swordBlade.position, attackRange);
    }

    // Start is called before the first frame update
    void Start()
    {
        dOTweenAnimation = GetComponentInChildren<DOTweenAnimation>();
    }

    // Update is called once per frame
    void Update()
    {

        movmentVector.x = Input.GetAxis("Right Stick X Axis");
        movmentVector.y = Input.GetAxis("Right Stick Y Axis");

        //TODO Cambia input attacco
        if (Input.GetButtonDown("Fire1"))
        {
            Attack();
        }


    }

    private void FixedUpdate()
    {
        if (movmentVector.x != 0.0f || movmentVector.y != 0.0f)
        {
        // CALCULATE ANGLE AND ROTATE


            swordAngle = Mathf.Atan2(movmentVector.x, movmentVector.y) * Mathf.Rad2Deg;

            // ANGLE GUN
            swordTransform.transform.rotation = Quaternion.AngleAxis(Mathf.Lerp(swordTransform.rotation.z, swordAngle,1), -Vector3.forward); //La spada ruota nel verso giusto

        }
    }

    void Attack()
    {
        //Sono andato a tempo?
        FindObjectOfType<MusicManager>().PlayTheBell();

        //La animazione comincia
        dOTweenAnimation.DORestart();


        //Quanti nemici ho colipito?
        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(swordBlade.position, attackRange, whatIsEnemy);
        for (int i = 0; i < enemiesToDamage.Length; i++)
        {
            enemiesToDamage[i].GetComponent<Enemy>().GetDamage(damage);
            Debug.Log("Hit");
        }
    }
}


