﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    //Dati giocatore
    public int playerHp;

    //Movimento del giocatore
    public Rigidbody2D rb;
    public float movementSpeed = 1; //Quanto veloce va il player
    public Vector2 movmentVector; //Direzione di movimento




    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        movmentVector.x = Input.GetAxis("Horizontal");
        movmentVector.y = Input.GetAxis("Vertical");
    }

    void FixedUpdate()
    {
        rb.velocity = movmentVector * movementSpeed;


    }


    //TODO Fare lo swipe del giocatore in base alla velocità




    //
    public void GetDamage(int enemyDamage = 1)
    {
        //Giocatore colpito da trappole e nemici
        playerHp--;

        if (playerHp < 1)
        {
            gameObject.SetActive(false);
            FindObjectOfType<GameManger>().LoadSceneByInt(0);
        }
    }

    public void OutOfTempo(int damage = 1)
    {
        //Giocatore combatte fuori tempo e perde vita
        if(playerHp > 1)
        {
            playerHp--;
            FindObjectOfType<Health>().lives--;
        }
    }

    public void OnTempo(int heal = 1)
    {
        //Giocatore combatte a tempo e ottiene vita
        if (playerHp < 3)
        {
            playerHp++;
            FindObjectOfType<Health>().lives++;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("Finish"))
        {
            GameObject.Find("Fader").GetComponent<DOTweenAnimation>().DOPlayById("FadeOut");
            //FindObjectOfType<GameManger>().LoadSceneByInt(0);
        }
    }
}
