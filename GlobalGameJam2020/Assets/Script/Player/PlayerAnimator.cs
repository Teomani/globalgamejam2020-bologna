﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class PlayerAnimator : MonoBehaviour
{
    public bool canChangeRoom = true;
    public PlayerController playerController;
    public SwordController swordController;
    Collider2D playerCollider;
    //
    public float cameraTransitionSpeed;
    public float playerTrabsitionSpeed;
    //
    public AudioSource playerAudioSource;
    public AudioClip[] steps;
    //

    // Start is called before the first frame update
    void Start()
    {
        playerController = GetComponent<PlayerController>();
        swordController = GetComponent<SwordController>();
        playerCollider = GetComponent<Collider2D>();

        if (!canChangeRoom)
        {
            canChangeRoom = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("RoomEntry"))
        {
            if (canChangeRoom)
            {
                Door newRoom = collision.gameObject.GetComponent<Door>();

                if (newRoom.isLocked == true)
                {
                    //Porta bloccata
                }
                else
                {
                    ChangeRoom(newRoom);
                    //Porta libera, muovi il personaggio
                }
            }
        }
    }

    public void ChangeRoom(Door newRoom)
    {
        canChangeRoom = false;

        //Anima il giocatore
        transform.DOMove(newRoom.roomEntry.position, playerTrabsitionSpeed)
                .OnStart(() => LockControls())
                .OnComplete(() => UnlockControls());


        //Animac la camera
        Camera.main.transform.DOMove(new Vector3(newRoom.cameraAncor.position.x, newRoom.cameraAncor.position.y,Camera.main.transform.position.z), cameraTransitionSpeed);
    }

    void LockControls()
    {
        //Blocca i comandi
        playerCollider.enabled = false;
        playerController.enabled = false;
        swordController.enabled = false;
    }

    void UnlockControls()
    {
        //Sblocca i comandi e attraversa le porte
        playerCollider.enabled = true;
        playerController.enabled = true;
        swordController.enabled = true;
        canChangeRoom = true;
    }

    public void PlayStepSound(int stepId)
    {
        playerAudioSource.clip = steps[stepId];
        playerAudioSource.Play();
    }
}
