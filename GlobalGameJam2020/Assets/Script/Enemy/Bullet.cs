﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int damage = 1;
    public float Attack_Range = 5;
    public LayerMask whatIsTarget;



    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, Attack_Range);
    }

    void FixedUpdate()
    {
        Collider2D hit2D = Physics2D.OverlapCircle(transform.position, Attack_Range, whatIsTarget);
        if (hit2D)
        {
            PlayerController player = hit2D.gameObject.GetComponent<PlayerController>();

            if (player)
            {
                //Prendi il giocatore e fagli danno
                player.GetDamage(damage);
                Destroy(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
       

    }
}
