﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class MeleeAI : MonoBehaviour
{
    public AIDestinationSetter aIDestinationSetter;
    //
    public enum EnemyState { Idle, Chase}
    public EnemyState enemyState;
    public LayerMask whatIsTarget;
    //
    public float FOV_Range = 10;
    public float Attack_Range = 5;
    //
    public Transform target;
    //
    public int damage = 1;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, Attack_Range);
        //
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, FOV_Range);
    }


    // Start is called before the first frame update
    void Start()
    {
        aIDestinationSetter = GetComponent<AIDestinationSetter>();
    }

    // Update is called once per frame
    void Update()
    {
       if(target == null || target.gameObject.active == false)
        {
            enemyState = EnemyState.Idle;
        }
        else
        {
            enemyState = EnemyState.Chase;
        }
    }

    void FixedUpdate()
    {
        if(enemyState == EnemyState.Idle)
        {
            //Cerca il giocatore 
            Collider2D hit2D = Physics2D.OverlapCircle(transform.position, FOV_Range, whatIsTarget);
            if (hit2D)
            {
                target = hit2D.gameObject.transform;
                aIDestinationSetter.target = hit2D.gameObject.transform;
                enemyState = EnemyState.Chase;
            }
        }

        if(enemyState == EnemyState.Chase)
        {
            //Se il giocatore è vicino lo attacca
            Collider2D hit2D = Physics2D.OverlapCircle(transform.position, Attack_Range, whatIsTarget);

            if (hit2D)
            {
                Debug.Log("Attack!!!");
                StartCoroutine("Attack");
            }
        }
    }

    IEnumerator Attack()
    {
        yield return new WaitForSeconds(1.5f);

        Collider2D hit2D = Physics2D.OverlapCircle(transform.position, Attack_Range, whatIsTarget);

        if (hit2D)
        {
            //Prendi il giocatore e fagli danno
            hit2D.gameObject.GetComponent<PlayerController>().GetDamage(damage);
        }
    }

}
