﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAI : MonoBehaviour
{

    public enum EnemyState { Hide, Idle, Attack}
    public EnemyState enemyState;
    //
    public LayerMask whatIsTarget;
    //
    public float FOV_Range = 10;
    //
    public Transform target;
    //
    public int damage = 1;
    //
    public List<Transform> spawPoint;
    //
    public GameObject bulletPrefab;
    public float bulletSpeed;
    /// <summary>
    /// 
    /// 
    /// </summary>
    [Header("Timing")]
    float timeToWaitBfShoot = 1;
    float timeToWaitBfHide = 1;

    private void OnDrawGizmos()
    {
        //
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, FOV_Range);
    }

    // Start is called before the first frame update
    void Start()
    {
        //Raccoglie tutti gli spawn point a stanza

        Collider2D hitInfo = Physics2D.OverlapCircle(transform.position, FOV_Range, whatIsTarget);

        if (hitInfo)
        {
            GameObject spawPointInRoom = hitInfo.transform.Find("Spawn").gameObject;

            if(spawPointInRoom != null)
            {
                for (int i = spawPoint.Count; i < spawPointInRoom.transform.childCount; i++)
                {
                    Debug.Log("Hit");
                    spawPoint.Add(spawPointInRoom.transform.GetChild(i));
                }
            }
        }

        target = GameObject.FindWithTag("Player").transform;

        ChooseRandomSpawPoint();
    }


    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    void ChooseRandomSpawPoint()
    {
        if (spawPoint.Count>0)
        {
            int Id = Random.Range(0, spawPoint.Count);

            transform.position = spawPoint[Id].position;
        }
   

        StartCoroutine("EnemyBehave");
    }


    IEnumerator EnemyBehave()
    {
        //Aspetta
        yield return new WaitForSeconds(timeToWaitBfShoot);
        //Spara
        Shoot();
        //Aspetta
        yield return new WaitForSeconds(timeToWaitBfHide);
        ChooseRandomSpawPoint();
    }

    void Shoot()
    {
        GameObject newBullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);

        Vector2 aimDirection = (target.position- transform.position);

        newBullet.GetComponent<Rigidbody2D>().AddForce(aimDirection * bulletSpeed,ForceMode2D.Impulse);


    }
}
