﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Health : MonoBehaviour
{

    public int lives; //Numero di vite attuali
    public int livesNumberMax; //Numero di vite massime

    public Image[] heartImages; //Oggetti che usiamo per mostrare le vite a schermo
    public Sprite fullBell; //Immagine del cuoricino pieno
    public Sprite brokenBell; //Immagine del cuoricino rotto

    // Start is called before the first frame update
    void Start()
    {
        lives = FindObjectOfType<PlayerController>().playerHp;
        //lives = livesNumber;
        UpdateLivesNumber();
    }


    public void UpdateLivesNumber()
    {
        //Con questo loop assegnamo gli sprite 
        for (int i = 0; i < heartImages.Length; i++)
        {
            if (i < lives)
            {
                //Assegan sprite
                heartImages[i].sprite = fullBell;
                //heartImages[i].GetComponent<DOTweenAnimation>().DOPlay();
            }
            else
            {
                heartImages[i].sprite = brokenBell;
                heartImages[i].GetComponent<DOTweenAnimation>().DOPause();
            }

            if (i < livesNumberMax)
            {
                heartImages[i].enabled = true;
            }
            else
            {
                heartImages[i].enabled = false;
            }
        }
    }

    //Anima i cuori secondo il ritmo della muscia
    public void Pulse()
    {
        //Con questo loop assegnamo gli sprite 
        for (int i = 0; i < heartImages.Length; i++)
        {
            if (i < lives)
            {
                //heartImages[i].sprite = fullBell;
                heartImages[i].GetComponent<DOTweenAnimation>().DORestart();
            }
        }
    }
}
