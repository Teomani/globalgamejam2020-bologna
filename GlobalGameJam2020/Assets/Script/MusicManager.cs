﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public float songBPM = 1f;
    public float endTiming = 0.5f;
    public bool isOnTime;
    public GameObject player;
    //
    public AudioSource weaponAudioSource;
    public AudioClip weaponSound;
    //
    public Health health;

    // Start is called before the first frame update
    void Start()
    {
        health = FindObjectOfType<Health>();
        InvokeRepeating("musicBeat", songBPM, songBPM);
        player = GameObject.FindWithTag("Player");

    }

    // Update is called once per frame
    //void Update()
    //{
    //    if(player != null || player.activeSelf == false)
    //    {
    //        if (Input.GetButtonDown("Fire1"))
    //        {
    //            if (isOnTime)
    //            {
    //                player.GetComponent<PlayerController>().OnTempo();
    //                BellSoundGood();
    //                Debug.Log("Good");
    //            }
    //            else if (!isOnTime)
    //            {
    //                player.GetComponent<PlayerController>().OutOfTempo();
    //                BellSoundBad();
    //                Debug.Log("Bad");
    //            }
    //        }
    //    }
    //}

    public void PlayTheBell()
    {
        if (isOnTime)
        {
            BellSoundGood();
            Debug.Log("Good");
        }
        else if (!isOnTime)
        {
            BellSoundBad();
            Debug.Log("Bad");
        }
    }

    void musicBeat()
    {
        health.Pulse();
        StartCoroutine("OnTime");
        Debug.Log("beat");
    }

    IEnumerator OnTime()
    {
        isOnTime = true;

        yield return new WaitForSeconds(endTiming);

        isOnTime = false;
    }

    //Il suono è riuscito
    void BellSoundGood()
    {
        weaponAudioSource.pitch = 1f;
        weaponAudioSource.Play();
        player.GetComponent<PlayerController>().OnTempo();
        health.UpdateLivesNumber();
    }

    //Hai sbagiato il tempo
    void BellSoundBad()
    {
        //Suoni
        weaponAudioSource.pitch = Random.Range(0.5f, 2f);
        weaponAudioSource.Play();
        //
        player.GetComponent<PlayerController>().OutOfTempo();
        health.UpdateLivesNumber();
    }
}
